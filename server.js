var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/clientes', function (req, res){

  res.sendFile(path.join(__dirname,'index.html')) ;
});

app.post('/clientes/:idcliente', function (req, res){
  res.send('Hemos modificado su cliente ' + req.params.idcliente);
});

app.post('/clientes/', function (req, res){
  res.send('Hemos modificado todos los clientes clientes');
});

app.delete('/clientes/:idcliente', function (req, res){
  res.send('Hemos borrado su cliente ' + req.params.idcliente);
});

app.delete('/clientes/', function (req, res){
  res.send('Hemos borrado todos los clientes');
});

app.put('/clientes/:idcliente', function (req, res){
  res.send('Hemos insertado su cliente'+ req.params.idcliente);
});
